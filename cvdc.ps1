using namespace System.Collections.Generic

Add-Type -assembly System.Windows.Forms
Add-Type -AssemblyName PresentationFramework

$ScriptVersion = "0.3.0"

$RunDir = (Get-Location).Path
$InstrumentInfoDir = (Get-Location).Path
$Global:ReagentsList = @(
    "Adt1X384",
    "EB",
    "ERA_Buff",
    "ERA_Enz",
    "EtOH",
    "FEBuff",
    "FEEnz",
    "FRAG_Buff_10X",
    "FRAG_Cond_Sol",
    "FRAG_Enz_5X",
    "KAPA_HiFi",
    "KAPA_Oligo",
    "KSINT_1",
    "KSPRI_1",
    "KSPRI_Lot_1",
    "KSTND",
    "L1DW_1",
    "L1DW_Lot_1",
    "LigaMM",
    "Liga_Buff",
    "Liga_Enz"
    "PCRMM",
    "PEAdt_1",
    "PELot_1",
    "PEOligo",
    "QIT_BUFF",
    "QSTOCK",
    "QuantiT",
    "QuantiT_Stock_1",
    "WATER_LOT",
    "WLigaSPRI_1",
    "Water"
)

$Global:ReagentHeadersList = [PSCustomObject] @{
    Watchmaker = @("Plate", "SN", "Script", "Current_User", "Tech", "Kit_Lot_Number", "Kit_Expire_Date", "Receive_Date", "Reagent_Lot_Number", "Expire_Date")
    KAPA = @("Plate", "SN", "Script", "Current_User", "Tech", "Reagent_Lot_Number", "Receive_Date", "Expire_Date")
}

# The list of instruments found in the run lots files
# The column is the instrument name, and the element is the serial number
$Global:RunInstrumentList = [PSCustomObject] @{
    TC_1     = [PSCustomObject] @{
        Instrument   = "Thermal cycler"
        Manufacturer = "Bio-Rad"
    }
    Thermo_1 = [PSCustomObject] @{
        Instrument   = "Thermal cycler"
        Manufacturer = "Bio-Rad"
    }
    M200_1   = [PSCustomObject] @{
        Instrument   = "Infinite Scanner M200"
        Manufacturer = "Tecan"
    }
}

function ChemistryType() {
    if ($RadioButtonWatchmaker.Checked) {
        return "Watchmaker"
    } elseif ($RadioButtonKAPA.Checked) {
        return "KAPA"
    }
}

function LogMessage($MsgLevel, $Message) {
    $TextBoxLog.AppendText("$(Get-Date -Format G): [$MsgLevel] $message`r`n")
    $TextBoxLog.ScrollToCaret()
}

function LogOutput($message) {
    LogMessage "INFO" $message
}

function LogWarning($message) {
    $TextBoxLog.Select($TextBoxLog.TextLength, 0)
    $TextBoxLog.SelectionColor = [System.Drawing.Color]::DarkOrange
    LogMessage "WARN" $message
}

function LogError($message) {
    Write-Error $message
    $TextBoxLog.Select($TextBoxLog.TextLength, 0)
    $TextBoxLog.SelectionColor = [System.Drawing.Color]::Red
    LogMessage "ERROR" $message
}

function ShowLogButtonClick {
    if ($TextBoxLog.Visible) {
        $MenuStripShowLogItem.Checked = $false
        $TextBoxLog.Visible = $false
        $MainForm.Height = $MainFormBaseHeight
    }
    else {
        $MenuStripShowLogItem.Checked = $true
        $TextBoxLog.Visible = $true
        $MainForm.Height = ($MainForm.Height + $TextBoxLog.Height)
    }
}

function ButtonInputClick {
    $TextBoxInput.Text = Get-Folder $TextBoxInput.Text
}

function ButtonOutputClick {
    $TextBoxOutput.Text = Get-Folder $TextBoxOutput.Text
}

function Get-Folder($initialDirectory = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select a lot root folder"
    $foldername.SelectedPath = $initialDirectory

    if ($foldername.ShowDialog() -eq "OK") {
        $folder += $foldername.SelectedPath
    }
    else {
        return $initialDirectory
    }
    return $folder
}

function ShowError($ErrorMsg) {
    [system.Windows.MessageBox]::Show($ErrorMsg, "ERROR", 0, 16)
}

function ShowHelpPopUp() {
    $HelpPopUp = New-Object System.Windows.Forms.Form
    $HelpPopUp.Text = "Help"
    $HelpPopUp.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterParent
    $HelpPopUp.MaximizeBox = $false
    $HelpPopUp.MinimizeBox = $false
    $HelpPopUp.AutoSize = $true
    $HelpPopUp.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink

    $HelpText = New-Object System.Windows.Forms.RichTextBox
    $HelpText.Rtf = "{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1033{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}{\f1\fnil\fcharset2 Symbol;}}
{\colortbl ;\red245\green174\blue0;\red255\green0\blue0;}
{\*\generator Riched20 10.0.19041}\viewkind4\uc1
\pard\sl240\slmult1\qc\f0\fs26\lang9 Clinical Validation Data Collator (CVDC) v$ScriptVersion\par
\fs18\'a9 Matthew Varga @ Ambry Genetics, 2021-2022\par

\pard\sl240\slmult1\ul\b\fs20\par
\fs24 Inputs\ulnone\b0\fs20\par

\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-180\li360\sl240\slmult1\b Run Lots:\b0  Folder containing the lot files, broken into sub folders for each run. Should contain files titled '*_lots.csv'.\par
{\pntext\f1\'B7\tab}\b Reagent Lots:\b0  Folder containing reagent lots as CSV files, one for each lot, titled as '*_lots*.csv'. Note: if duplicates (based on plate, serial number, and lot number) are discovered,only one is used.\par
{\pntext\f1\'B7\tab}\b Output Folder: \b0 Folder to which the resulting collated tables should be written.\par

\pard\sl240\slmult1\ul\b\par
\fs24 Usage\ulnone\b0\fs20\par
Denote the run lots, reagent lots, and output folders by browsing to them with the 'Browse' button. Press Run, and a CSV file containing the collated tables will be written to the output folder, one for each run lot.\par
\par
\ul\b\fs24 Options\ulnone\b0\fs20\par

\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent0{\pntxtb\'B7}}\fi-360\li720\sl240\slmult1\b Reagents List\b0 : A list of all Run Lot column names that will be treated as reagent names. The program will search for these, \ul and only these\ulnone , reagents.\par
{\pntext\f1\'B7\tab}\b Show Logs\b0 : Displays a window below the run button which shows potentially useful log messages during collation. There are several levels of information: INFO (basic information), \cf1 WARN\cf0  (warnings pertaining to non-critical issues), \cf2 ERROR\cf0  (errors, such as missing or improperly formatted critical files).\par
}"
    $HelpText.Size = New-Object System.Drawing.Size(400, 300)
    $HelpText.Location = New-Object System.Drawing.Point(0, 0)
    $HelpText.BorderStyle = [System.Windows.Forms.BorderStyle]::None
    $HelpText.SelectionProtected = $true
    $HelpText.ReadOnly = $true

    $HelpPopUp.Controls.Add($HelpText)
    $HelpPopUp.ShowDialog()
}

function ManufacturerFromModel($Model) {
    if ($Model.Contains("Freedom") -or $Model.Contains("Fluent")) {
        return "Tecan"
    }
    return ""
}

function CreateOutputCSV($Data) {
    $OutStr = "Date,$(Get-Date -Format G)`r`n"
    $OutStr += "User,$($env:USERNAME)`r`n"
    $OutStr += "Domain,$($env:USERDOMAIN)`r`n"
    $Outstr += "Script_Version,$ScriptVersion`r`n"
    $OutStr += "Chemistry,$(ChemistryType)`r`n"
    $OutStr += "`r`n"
    ForEach ($Elem in $Data) {
        $OutStr += $Elem.Name
        if (-not ($Elem.Data.Count -eq 0)) {
            ForEach ($Line in ($Elem.Data | ConvertTo-Csv -NoTypeInformation)) {
                $OutStr = ($OutStr, $Line -join "`r`n")
            }
        }
        $OutStr += "`r`n`r`n"
    }
    return $OutStr
}

function ReadInstrumentInfo($filename) {
    try {
        if ((Test-Path "$filename.xlsm")) {
            $filename = "$filename.xlsm"
        }
        elseif ((Test-Path "$filename.xlsx")) {
            $filename = "$filename.xlsx"
        }
        else {
            Throw [System.IO.IOException]::new()
        }
        $InstrumentInfoFilename = "$InstrumentInfoDir\$filename"
        LogOutput "Reading instrument information from '$InstrumentInfoFilename'"
        $Excel = New-Object -Com Excel.Application
        $Excel.DisplayAlerts = $false
        $Sheet = $Excel.Workbooks.Open((Get-Item $InstrumentInfoFilename).FullName).Sheets.Item(1)
        $Instruments = ForEach ($Row in ($Sheet.UsedRange.Rows | Select-Object -Skip 2)) {
            New-Object PSObject -Property @{
                "SN"             = $Row.Cells.Item(2).Value2
                "Model"          = $Row.Cells.Item(3).Value2
                "Software_Build" = $Row.Cells.Item(5).Value2
                "Touch_Tools"    = $Row.Cells.Item(8).Value2
                "Manufacturer"   = (ManufacturerFromModel $Row.Cells.Item(3).Value2)
            }
        }
        $Excel.Quit()
    }
    catch [System.IO.IOException], [System.Runtime.InteropServices.COMException] {
        ShowError "Could not find '$filename.xlsm' or '$filename.xlsx'. Please place it in the same folder as this script."
        Exit 1
    }
    return $Instruments
}

function ConvertReagentDate($InDate) {
    if ($null -ne $InDate) {
        try {
            return [datetime]::ParseExact($InDate, "yyMMdd", $null).ToString("MM/dd/yyyy")
        }
        catch {
            LogError "Could not parse '$InDate' as a date."
            return $InDate
        }
    }
    return $null
}

function BuildReagentsMap($ReagentDirectory) {
    LogOutput "Building reagent lots database from '$ReagentDirectory'"
    $ReagentHeader = $Global:ReagentHeadersList | Select-Object -ExpandProperty $(ChemistryType)
    $ReagentFiles = Get-ChildItem (Join-Path $ReagentDirectory -ChildPath "*_lots*.csv")
    if ($ReagentFiles.Length -eq 0) {
        $WarningMsg = [System.Windows.MessageBox]::Show(
            "Reagent lot folder contains no reagent lot files ('*_lots.csv'). Continue without reagent lots files?",
            "Warning",
            "YesNo",
            48
        )
        if ($WarningMsg -eq "No") {
            LogOutput "Cancelling run due to missing reagents lots files."
            return "Cancel"
        }
        else {
            $ReagentFiles = @()
            LogOutput "Continuing run without reagents lots files."
        }
    }

    # Read the reagent lot files, and select unique entries
    $Reagents = [List[PSCustomObject]]::new($ReagentFiles.Length)
    ForEach ($ReagentFilename in $ReagentFiles) {
        $Reagent = Import-Csv $ReagentFilename.FullName -Header $ReagentHeader | Select-Object -Skip 1
        if ($Reagent.Reagent_Lot_Number) {
            $Reagents += $Reagent
        } else {
            LogError "Could not read reagent lot number from file '$ReagentFilename'."
        }
    }
    $Reagents = $Reagents | Select-Object -Unique -Property Plate,SN,Tech,Reagent_Lot_Number,Receive_Date,Expire_Date
    return $Reagents
}

function GetObjectKeys($Obj) {
    # Get the keys of a PSCustomObject (treating it as if it were a dictionary)
    return $Obj | Get-Member -MemberType NoteProperty | Select-Object -ExpandProperty Name
}

function AddReagentsUsedRow($Script, $Plate, $ReagentName, $ReagentInfo) {
    return [PSCustomObject] [ordered] @{
        "Workflow Step"       = $Script
        "Reagent"             = $ReagentName
        "Stock Concentration" = ""
        "Manufacturer"        = ""
        "Catalog Number"      = ""
        "Plate"               = $Plate
        "SN"                  = $ReagentInfo.SN
        "Lot Number"          = $ReagentInfo.Reagent_Lot_Number
        "Receive Date"        = ConvertReagentDate $ReagentInfo.Receive_Date
        "Expiry Date"         = ConvertReagentDate $ReagentInfo.Expire_Date
        "Tech"                = $ReagentInfo.Tech
        "Storage Condition"   = ""
    }
}

function MineData($Instruments, $Reagents, $LotDirectory) {
    $RunInstrumentNames = GetObjectKeys $Global:RunInstrumentList
    $InitialCapacity = 20
    $Tables = @(
        [PSCustomObject]@{ "Name" = "Table 16.1 - Equipment Used"; "Data" = [List[PSCustomObject]]::new($InitialCapacity) },
        [PSCustomObject]@{ "Name" = "Table 16.2 - Reagents Used"; "Data" = [List[PSCustomObject]]::new($InitialCapacity) },
        [PSCustomObject]@{ "Name" = "Table 16.3 - Consumables Used"; "Data" = [List[PSCustomObject]]::new($InitialCapacity) },
        [PSCustomObject]@{ "Name" = "Table 16.4 - Software Used"; "Data" = [List[PSCustomObject]]::new($InitialCapacity) },
        [PSCustomObject]@{ "Name" = "Table 16.5 - Operators"; "Data" = [List[PSCustomObject]]::new($InitialCapacity) }
    )

    $LotFiles = Get-ChildItem (Join-Path $LotDirectory -ChildPath "*_lots.csv")
    if ($LotFiles.Length -eq 0) {
        ShowError "Lot Folder contains no lot files ('*_lots.csv')."
        Exit 1
    }
    ForEach ($LotFilename in $LotFiles ) {
        LogOutput "Reading from $LotFilename"
        $LotData = Import-Csv $LotFilename.FullName
        ForEach ($Element in $LotData) {
            $ElementNames = GetObjectKeys $Element
            $Instrument = $Instruments.Where({ $PSItem.SN -eq $Element.SN })
            $Tables[0].Data += [PSCustomObject] [ordered] @{
                "Workflow Step" = $Element.Script
                "Instrument"    = $Instrument.Model
                "Manufacturer"  = $Instrument.Manufacturer
                "Serial Number" = $Element.SN
            }
            # Get the thermal cycler instrument information from the run lot, using the hard-coded information in $Global:RunInstrumentList
            ForEach ($RunInstrument in (Compare-Object $RunInstrumentNames $ElementNames -PassThru -IncludeEqual -ExcludeDifferent)) {
                $TmpInstrument = $Global:RunInstrumentList | Select-Object -ExpandProperty $RunInstrument
                $Tables[0].Data += [PSCustomObject] [ordered] @{
                    "Workflow Step" = $Element.Script
                    "Instrument"    = $TmpInstrument.Instrument
                    "Manufacturer"  = $TmpInstrument.Manufacturer
                    "Serial Number" = $Element | Select-Object -ExpandProperty $RunInstrument
                }
            }
            ForEach ($ReagentName in $Global:ReagentsList) {
                if ($Element | Get-Member $ReagentName) {
                    $Plate = ($Element | Select-Object -ExpandProperty $ReagentName)
                    $ReagentMatches = $Reagents.Where({ $PSItem.Plate -eq $Plate })
                    if ($ReagentMatches) {
                        ForEach ($Match in $ReagentMatches) {
                            $Tables[1].Data += AddReagentsUsedRow $Element.Script $Plate $ReagentName $Match
                        }
                    } else {
                        LogWarning "Missing reagent lot file for plate '$Plate' reagent '$ReagentName'."
                        $Tables[1].Data += AddReagentsUsedRow $Element.Script $Plate $ReagentName
                    }
                }
            }
            $Tables[2].Data += [PSCustomObject] [ordered] @{
                "Workflow Step"  = $Element.Script
                "Consumable"     = ""
                "Manufacturer"   = ""
                "Catalog Number" = ""
            }

            # The software names here are hard-coded, based on information from Lernni Juco
            $SoftwareName = ""
            if ($Instrument.Model -Like "*EVO*") {
                $SoftwareName = "EVOWare"
                $Tables[3].Data += [PSCustomObject]@{
                    "Workflow Step"  = $Element.Script
                    "Software"       = "Touch Tools"
                    "Manufacturer"   = $Instrument.Manufacturer
                    "Version Number" = $Instrument.Touch_Tools
                }
            }
            elseif ($Instrument.Model -Like "*Fluent*") {
                $SoftwareName = "Fluent Control"
            }
            $Tables[3].Data += [PSCustomObject]@{
                "Workflow Step"  = $Element.Script
                "Software"       = $SoftwareName
                "Manufacturer"   = $Instrument.Manufacturer
                "Version Number" = $Instrument.Software_Build
            }
            $Tables[4].Data += [PSCustomObject]@{
                "Workflow Step" = $Element.Script
                "Name"          = $Element.TECH
                "Title"         = ""
                "Team"          = ""
                "Role"          = ""
            }
        }
    }
    ForEach ($Elem in $Tables) {
        if (-not ($Elem.Data.Count -eq 0)) {
            $Elem.Data = $Elem.Data | Sort-Object -Property "Workflow Step"
        }
    }
    
    return $Tables
}

function RunButtonClick($LotDirectory, $ReagentsDirectory, $OutputDirectory) {
    LogOutput "Running with $(ChemistryType) chemistry"
    $LotDirList = (Get-ChildItem $LotDirectory).Where({ $PSItem.Mode -eq "d-----" })
    if ($LotDirList.Count -eq 0) {
        [System.Windows.MessageBox]::Show("The chosen folder has no valid lot folders.", "Warning", "OK", "Warning")
        return "Cancel"
    }
    else {
        LogOutput "Found $($LotDirList.Count) directories."
        $Reagents = BuildReagentsMap $ReagentsDirectory
        if ($Reagents -eq "Cancel") {
            return "OK"
        }
        $Instruments = ReadInstrumentInfo "Instrument Info"
        ForEach ($Lot in $LotDirList) {
            LogOutput "Working on $($Lot.Name)"
            $Tables = MineData $Instruments $Reagents $Lot.FullName

            $OutputFilename = (Join-Path $OutputDirectory -ChildPath "$($Lot.Name).csv")
            LogOutput "Writing to $OutputFilename"
            CreateOutputCSV $Tables | Out-File -Encoding utf8 -FilePath $OutputFilename
        }
    }

    LogOutput "Collation complete!"
    [System.Windows.MessageBox]::Show("Done!", "Status", "OK", "Exclamation")
    return "OK"
}

[System.Windows.Forms.Application]::EnableVisualStyles()

$MainFont = New-Object System.Drawing.Font("Segoe UI", 9.75)
$BoldFont = New-Object System.Drawing.Font("Segoe UI", 9.75, [System.Drawing.FontStyle]::Bold)

$LeftPad = 10
$TopPad = 25
$InternalPad = 5

$MainForm = New-Object System.Windows.Forms.Form
$MainFormBaseHeight = 220
$MainForm.Text = "CVDC"
$MainForm.Width = 580
$MainForm.Height = $MainFormBaseHeight
$MainForm.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen
$MainForm.MaximizeBox = $false
$MainForm.MinimizeBox = $false

$FileSelectLabelSize = New-Object System.Drawing.Size(100, 25)
$FileSelectTextBoxSize = New-Object System.Drawing.Size(350, 25)
$FileSelectButtonSize = New-Object System.Drawing.Size(70, 25)

$Label = New-Object System.Windows.Forms.Label
$Label.Text = "Run Lots:"
$Label.Location = New-Object System.Drawing.Size($LeftPad, $TopPad)
$Label.Size = $FileSelectLabelSize
$Label.TextAlign = [System.Drawing.ContentAlignment]::MiddleRight
$Label.Font = $BoldFont

$TextBoxInput = New-Object System.Windows.Forms.TextBox
$TextBoxInput.Multiline = $false
$TextBoxInput.Location = New-Object System.Drawing.Size(($Label.Bounds.Right + $InternalPad), $TopPad)
$TextBoxInput.ShortcutsEnabled = $true
$TextBoxInput.Size = $FileSelectTextBoxSize
$TextBoxInput.Text = $RunDir
$TextBoxInput.Font = $MainFont

$ButtonInput = New-Object System.Windows.Forms.Button
$ButtonInput.Location = New-Object System.Drawing.Size(($TextBoxInput.Bounds.Right + $InternalPad), $TopPad)
$ButtonInput.Size = $FileSelectButtonSize
$ButtonInput.Text = "Browse"
$ButtonInput.Font = $MainFont

$ReagentsFolderRowY = ($Label.Bounds.Bottom + $InternalPad)
$LabelReagents = New-Object System.Windows.Forms.Label
$LabelReagents.Text = "Reagent Lots:"
$LabelReagents.Location = New-Object System.Drawing.Size($LeftPad, $ReagentsFolderRowY)
$LabelReagents.Size = $FileSelectLabelSize
$LabelReagents.TextAlign = [System.Drawing.ContentAlignment]::MiddleRight
$LabelReagents.Font = $BoldFont

$TextBoxReagents = New-Object System.Windows.Forms.TextBox
$TextBoxReagents.Multiline = $false
$TextBoxReagents.Location = New-Object System.Drawing.Size(($LabelReagents.Bounds.Right + $InternalPad), $ReagentsFolderRowY)
$TextBoxReagents.Size = $FileSelectTextBoxSize
$TextBoxReagents.Text = $RunDir
$TextBoxReagents.Font = $MainFont

$ButtonReagents = New-Object System.Windows.Forms.Button
$ButtonReagents.Location = New-Object System.Drawing.Size(($TextBoxReagents.Bounds.Right + $InternalPad), $ReagentsFolderRowY)
$ButtonReagents.Size = $FileSelectButtonSize
$ButtonReagents.Text = "Browse"
$ButtonReagents.Font = $MainFont

$SecondRowY = ($LabelReagents.Bounds.Bottom + $InternalPad)

$LabelOutput = New-Object System.Windows.Forms.Label
$LabelOutput.Text = "Output Folder:"
$LabelOutput.Location = New-Object System.Drawing.Size($LeftPad, $SecondRowY)
$LabelOutput.Size = $FileSelectLabelSize
$LabelOutput.TextAlign = [System.Drawing.ContentAlignment]::MiddleRight
$LabelOutput.Font = $BoldFont

$TextBoxOutput = New-Object System.Windows.Forms.TextBox
$TextBoxOutput.Multiline = $false
$TextBoxOutput.Location = New-Object System.Drawing.Size(($LabelOutput.Bounds.Right + $InternalPad), $SecondRowY)
$TextBoxOutput.Size = $FileSelectTextBoxSize
$TextBoxOutput.Text = $RunDir
$TextBoxOutput.Font = $MainFont

$ButtonOutput = New-Object System.Windows.Forms.Button
$ButtonOutput.Location = New-Object System.Drawing.Size(($TextBoxOutput.Bounds.Right + $InternalPad), $SecondRowY)
$ButtonOutput.Size = $FileSelectButtonSize
$ButtonOutput.Text = "Browse"
$ButtonOutput.Font = $MainFont

$ThirdRowY = ($LabelOutput.Bounds.Bottom + $InternalPad)

$LabelChemistrySelection = New-Object System.Windows.Forms.Label
$LabelChemistrySelection.Location = New-Object System.Drawing.Size($LeftPad, $ThirdRowY)
$LabelChemistrySelection.Text = "Chemistry:"
$LabelChemistrySelection.Size = $FileSelectLabelSize
$LabelChemistrySelection.TextAlign = [System.Drawing.ContentAlignment]::MiddleRight
$LabelChemistrySelection.Font = $BoldFont

$RadioButtonWatchmaker = New-Object System.Windows.Forms.RadioButton
$RadioButtonWatchmaker.Location = New-Object System.Drawing.Point(0,0)
$RadioButtonWatchmaker.Checked = $true
$RadioButtonWatchmaker.Text = "Watchmaker"
$RadioButtonWatchmaker.UseVisualStyleBackColor = $true
$RadioButtonWatchmaker.Size = New-Object System.Drawing.Size(90, 30)

$RadioButtonKAPA = New-Object System.Windows.Forms.RadioButton
$RadioButtonKAPA.Location = New-Object System.Drawing.Point(($RadioButtonWatchmaker.Bounds.Right + $InternalPad), 0)
$RadioButtonKAPA.Checked = $false
$RadioButtonKAPA.Text = "KAPA"
$RadioButtonKAPA.UseVisualStyleBackColor = $true
$RadioButtonKAPA.Size = New-Object System.Drawing.Size(60, 30)

$ChemistrySelectionPanel = New-Object System.Windows.Forms.Panel
$ChemistrySelectionPanel.Location = New-Object System.Drawing.Size(($LabelChemistrySelection.Bounds.Right + $InternalPad), $ThirdRowY)
$ChemistrySelectionPanel.Size = $FileSelectTextBoxSize

$ChemistrySelectionPanel.Controls.AddRange(@($RadioButtonWatchmaker, $RadioButtonKAPA))

$FourthRowY = ($ChemistrySelectionPanel.Bounds.Bottom + $InternalPad)
$FourthRowButtonSize = New-Object System.Drawing.Size(($MainForm.Width - ($LeftPad * 4)), 30)

$ButtonRun = New-Object System.Windows.Forms.Button
$ButtonRun.Location = New-Object System.Drawing.Size($LeftPad, $FourthRowY)
$ButtonRun.Size = $FourthRowButtonSize
$ButtonRun.Text = "Run!"
$ButtonRun.Font = $MainFont

$TextBoxLog = New-Object System.Windows.Forms.RichTextBox
$TextBoxLog.Location = New-Object System.Drawing.Size($LeftPad, ($ButtonRun.Bounds.Bottom + $InternalPad))
$TextBoxLog.Size = New-Object System.Drawing.Size(($MainForm.Width - ($LeftPad * 4)), 200)
$TextBoxLog.AutoSize = $false
$TextBoxLog.ReadOnly = $true
$TextBoxLog.Multiline = $true
$TextBoxLog.WordWrap = $false
$TextBoxLog.ScrollBars = "Both"
$TextBoxLog.Visible = $false
$TextBoxLog.Font = New-Object System.Drawing.Font("Courier New", 8)

$MenuStripReagentsItem = New-Object System.Windows.Forms.ToolStripMenuItem
$MenuStripReagentsItem.Text = "Reagents List"
$MenuStripReagentsItem.ToolTipText = "Show the comprehensive list of reagents which are searched for."
$MenuStripReagentsItem.Add_Click({
    $PopUp = New-Object System.Windows.Forms.Form
    $PopUp.Text = "Reagents"
    $PopUp.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterParent
    $PopUp.MaximizeBox = $false
    $PopUp.MinimizeBox = $false
    $PopUp.AutoSize = $true
    $PopUp.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink

    $ListBox = New-Object System.Windows.Forms.ListBox
    $ListBox.Size = New-Object System.Drawing.Size(200, 200)
    $ListBox.Location = New-Object System.Drawing.Point(0, 0)
    $ListBox.Items.AddRange($Global:ReagentsList)

    $PopUp.Controls.Add($ListBox)
    $PopUp.ShowDialog()
})

$MenuStripHelpItem = New-Object System.Windows.Forms.ToolStripMenuItem
$MenuStripHelpItem.Text = "Help"

$MenuStripSeparatorItem = New-Object System.Windows.Forms.ToolStripSeparator

$MenuStripExitItem = New-Object System.Windows.Forms.ToolStripMenuItem
$MenuStripExitItem.Text = "Exit"
$MenuStripExitItem.Add_Click({ $MainForm.Close() })

$MenuStripShowLogItem = New-Object System.Windows.Forms.ToolStripMenuItem
$MenuStripShowLogItem.Text = "Show logs"
$MenuStripShowLogItem.ToolTipText = "Show log messages in a panel as the script run."
$MenuStripShowLogItem.Checked = $false

$MenuStripBaseItem = New-Object System.Windows.Forms.ToolStripMenuItem
[void]$MenuStripBaseItem.DropDownItems.AddRange(
    @(
        $MenuStripReagentsItem,
        $MenuStripHelpItem,
        $MenuStripSeparatorItem,
        $MenuStripExitItem
    )
)
$MenuStripBaseItem.Text = "File"
$MenuStripBaseItem.Name = "MenuStripBaseItem"

$MenuStripBaseItem2 = New-Object System.Windows.Forms.ToolStripMenuItem
$MenuStripBaseItem2.Text = "Options"
[void]$MenuStripBaseItem2.DropDownItems.Add($MenuStripShowLogItem)
$MenuStripBaseItem2.Name = "MenuStripBaseItem2"

$MenuStrip = New-Object System.Windows.Forms.MenuStrip
[void]$MenuStrip.Items.AddRange(
    @(
        $MenuStripBaseItem,
        $MenuStripBaseItem2
    )
)
$MenuStrip.Location = New-Object System.Drawing.Point(0, 0)
$MenuStrip.Name = "MenuStrip"
$MenuStrip.Size = New-Object System.Drawing.Size($MainForm.Width, 15)
$MenuStrip.TabIndex = 0
$MenuStrip.Text = "MenuStrip"

$ToolTip = New-Object System.Windows.Forms.ToolTip
$ToolTip.SetToolTip($Label, "Folder containing the run lots CSV files.")
$ToolTip.SetToolTip($LabelReagents, "Folder containing the reagent lots CSV files.")
$ToolTip.SetToolTip($LabelOutput, "Folder to which the compiled tables will be written.")
$ToolTip.SetToolTip($LabelChemistrySelection, "Set the chemistry of the run lots.")

$MainForm.Controls.AddRange(
    @(
        $Label,
        $ButtonInput,
        $TextBoxInput,
        $LabelReagents,
        $TextBoxReagents,
        $ButtonReagents,
        $LabelOutput,
        $ButtonOutput,
        $TextBoxOutput,
        $LabelChemistrySelection,
        $ChemistrySelectionPanel,
        $ButtonRun,
        $TextBoxLog,
        $MenuStrip
    )
)
$ButtonInput.Add_Click({ ButtonInputClick })
$ButtonReagents.Add_Click({ $TextBoxReagents.Text = Get-Folder $TextBoxReagents.Text })
$ButtonOutput.Add_Click({ ButtonOutputClick })
$ButtonRun.Add_Click({ RunButtonClick $TextBoxInput.Text $TextBoxReagents.Text $TextBoxOutput.Text })
$MenuStripShowLogItem.Add_Click({ ShowLogButtonClick })
$MenuStripHelpItem.Add_Click({ ShowHelpPopUp })
$MainForm.ShowDialog()